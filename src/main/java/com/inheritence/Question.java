package com.inheritence;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tbl_question")
public class Question {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Quesid;    
	private String qname;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(
            name = "Question_Answer",
            joinColumns = @JoinColumn(name = "Questin_id",referencedColumnName="Quesid"),
            inverseJoinColumns = @JoinColumn(name = "Answer_Id",referencedColumnName="ansId")
    )
	//@JoinColumn(name="questionId",referencedColumnName="id")
	private List<Answer> answers;
	public List<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
	
	public int getQuesid() {
		return Quesid;
	}
	public void setQuesid(int quesid) {
		Quesid = quesid;
	}
	public String getQname() {
		return qname;
	}
	public void setQname(String qname) {
		this.qname = qname;
	}  

}
