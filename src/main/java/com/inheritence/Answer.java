package com.inheritence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_answer")

public class Answer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	 private int ansId;    
	    private String answername;    
	    private String postedBy;
		
		public int getAnsId() {
			return ansId;
		}
		public void setAnsId(int ansId) {
			this.ansId = ansId;
		}
		public String getAnswername() {
			return answername;
		}
		public void setAnswername(String answername) {
			this.answername = answername;
		}
		public String getPostedBy() {
			return postedBy;
		}
		public void setPostedBy(String postedBy) {
			this.postedBy = postedBy;
		}
}
