package com.inheritence;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class QuestionMain {
	public static void main(String[] args) {
		
		
		Configuration configuration = new Configuration();
	    // configuring hibernate
	    SessionFactory sessionFactory = configuration.configure().buildSessionFactory();
	    Session session = sessionFactory.openSession();
	    Transaction tx = session.beginTransaction();
	    Question quest=new Question();
	    quest.setQname("What is Java");
	    
	    Answer ans1=new Answer();
	    ans1.setAnswername("Java is a programming language");
	    ans1.setPostedBy("James");
	    
	    Answer ans2=new Answer();
	    ans2.setAnswername("Java is a platform");
	    ans2.setPostedBy("Gosling");
	    
	    List<Answer> list=new ArrayList<Answer>();
	    list.add(ans1);
	    list.add(ans2);
	    
	    quest.setAnswers(list);
	    
	    session.save(quest);
	    tx.commit();
	    
	    
	 
	}
}
