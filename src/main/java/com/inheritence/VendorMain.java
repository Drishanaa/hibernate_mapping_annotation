package com.inheritence;

import java.util.LinkedHashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class VendorMain {
	public static void main(String[] args) {
		
	
	Configuration configuration = new Configuration();
    // configuring hibernate
    SessionFactory sessionFactory = configuration.configure().buildSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction tx = session.beginTransaction();
  Vendor vendor=new Vendor();
  vendor.setVendorId(1002);
  vendor.setVendorName("NIIT");
  
  //customer1
  
  Customers cust1=new Customers();
  cust1.setCustomerId(1);
  cust1.setCustomerName("hcl");
  //customer2
  
  Customers cust2=new Customers();
  cust2.setCustomerId(2);
  cust2.setCustomerName("wipro");
  
  //adding customers details to set
  Set<Customers> customers=new LinkedHashSet<Customers>();
  customers.add(cust1);
  customers.add(cust2);
  
  //adding set of customers to vendor object
  vendor.setCustomers(customers);
  
  //saving the data
session.save(vendor);
//commit
tx.commit();
	}
}

    

